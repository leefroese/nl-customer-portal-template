import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Provider } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { PageScrollConfig } from 'ngx-page-scroll';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AgreementsSummaryComponent } from './agreements-summary/agreements-summary.component';
import { BillingHistoryComponent } from './billing-history/billing-history.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountDetailsComponent } from './account-details/account-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AgreementsSummaryComponent,
    BillingHistoryComponent,
    ProfileComponent,
    AccountDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    IconSpriteModule,
    NgxPageScrollModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    PageScrollConfig.defaultDuration = 500;
    PageScrollConfig.defaultScrollOffset = 100;
  }

}