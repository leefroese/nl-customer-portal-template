import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementsSummaryComponent } from './agreements-summary.component';

describe('AgreementsSummaryComponent', () => {
  let component: AgreementsSummaryComponent;
  let fixture: ComponentFixture<AgreementsSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreementsSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementsSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
