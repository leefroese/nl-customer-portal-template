import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgreementsSummaryComponent } from './agreements-summary/agreements-summary.component';
import { BillingHistoryComponent } from './billing-history/billing-history.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountDetailsComponent } from './account-details/account-details.component';

const routes: Routes = [
  {
    path: 'agreements-summary',
    component: AgreementsSummaryComponent
  },
  {
    path: 'billing-history',
    component: BillingHistoryComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'account-details',
    component: AccountDetailsComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
